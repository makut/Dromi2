﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace Dromi.Models
{
    public class DromiContext:DbContext
    {

        public DromiContext(DbContextOptions<DromiContext> options)
                : base(options)
        {
        }

        //public DromiContext(DromiContext context)
        //{
        //    _context = context;
        //}
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<AvailableCategories> AvailableCategories { get; set; }
        public DbSet<Categories> Category { get; set; }
        public DbSet<Reviews> Review { get; set; }
        public DbSet<Gallery> Images { get; set; }
    }
}
