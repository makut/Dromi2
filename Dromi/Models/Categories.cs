﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dromi.Models
{
    public class Categories
    {
        public int CategoriesId { get; set; }
        public string CategoryName { get; set; }

    }
}
