﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dromi.Models
{
    public class AvailableCategories
    {
        public int AvailableCategoriesId { get; set; }
        
        public string AvailCatName { get; set; }

        public ICollection<Profile> Profiles { get; set; } 

    }
}
