﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Commons;
using Dromi.Commons;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Dromi.Models;
using Npgsql;

namespace Dromi.Controllers
{
    [Route("profile")]
    [ApiController]
    public class ProfilesController : ControllerBase
    {
        private readonly DromiContext _context;

        public ProfilesController(DromiContext context)
        {
            _context = context;
        }

        // GET: api/Profiles
        [HttpGet]
        public IEnumerable<Profile> GetProfiles()
        {
            var profile = _context.Profiles.Include(r => r.Reviews).Include(i => i.Images);

            return profile;
        }
           

        // GET: api/Profiles/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProfile([FromRoute] int id)
        {
            Profile findId = null;
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<Profile>( ));
            }     
            using (var data = _context)
            {
                var getProfile = _context.Profiles.Find(id);
                if (getProfile == null)
                {
                    return StatusCode((int)HttpStatusCode.NotFound, new ApiResponse<Profile>(getProfile,
                        CommonStatusCode.STATUS_CODE_SUCCESS, CommonStatusCode.STATUS_CODE_SUCCESS_MESSAGE
                    ));

                }

                if (id!=getProfile.ProfileId)
                {
                    return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<dynamic>()
                    {
                        Status = CommonStatusCode.STATUS_CODE_FAILED,
                        Message = "Id is not correct"
                        
                    });
                   // DromiLogs.Error<Profile>("");
                }
                data.Entry(getProfile).Collection(pro => pro.Reviews).Load();
                return StatusCode((int)HttpStatusCode.OK, new ApiResponse<Profile>(getProfile,
                    CommonStatusCode.STATUS_CODE_SUCCESS, CommonStatusCode.STATUS_CODE_SUCCESS_MESSAGE
                ));

            }
        }

        // PUT: api/Profiles/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] Profile profile)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<Profile>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            if (id != profile.AvailableCategoriesId)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<Profile>()
                {
                    Message = "Id is not correct",
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            _context.Entry(profile).State = EntityState.Modified;

            try
            {
                var add = await _context.SaveChangesAsync();

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProfileExists(id))
                {
                    return StatusCode((int)HttpStatusCode.NotFound, new ApiResponse<Profile>(
                        profile,
                        CommonStatusCode.STATUS_CODE_FAILED, CommonStatusCode.NOT_FOUND_ERROR_MESSAGE));
                }
                else
                {
                    throw;
                }
            }

            return StatusCode((int)HttpStatusCode.OK, new ApiResponse<Profile>()
            {
                Status = CommonStatusCode.STATUS_CODE_SUCCESS,
                Message = CommonStatusCode.STATUS_CODE_SUCCESS_MESSAGE
            });
        }

        // POST: api/Profiles
        [HttpPost]
        public async Task <IActionResult> Create([FromBody] Profile profile)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");

                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<dynamic>
                {
                    Status = CommonStatusCode.STATUS_CODE_FAILED,
                    Message = message
                });
            }

               

            try
            {
                _context.Profiles.Add(profile);
                await _context.SaveChangesAsync();
                return StatusCode((int)HttpStatusCode.Created, new ApiResponse<Profile>(profile,
                    CommonStatusCode.STATUS_CODE_SUCCESS,
                    CommonStatusCode.STATUS_CODE_SUCCESS));
            }

            catch (DbUpdateException ex)
            {
                DromiLogs.Error<ProfilesController>(ex, ex.StackTrace);

                var pgException = (PostgresException)ex.InnerException;

                if (pgException != null)
                {
                    var exResponse = new ApiResponse<string>(null,
                        CommonStatusCode.STATUS_CODE_FAILED,
                        pgException.Detail);
                    return StatusCode((int)HttpStatusCode.InternalServerError, exResponse);
                }

                var response = new ApiResponse<string>(null,
                    CommonStatusCode.STATUS_CODE_FAILED,
                    ex.InnerException.ToString());
                return StatusCode((int)HttpStatusCode.InternalServerError, response);
            }
            catch (Exception ex)
            {
                DromiLogs.Error<ProfilesController>(ex, ex.ToString());
                var response = new ApiResponse<string>(string.Empty,
                    CommonStatusCode.STATUS_CODE_FAILED,
                    CommonStatusCode.INTERNAL_SERVER_ERROR_MESSAGE);

                return StatusCode((int)HttpStatusCode.InternalServerError, response);

            }
        }

        // DELETE: api/Profiles/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProfile([FromRoute] int id)
        {
            var message = ModelState.GetErrorMessages(",");
            if (!ModelState.IsValid)
            {
                return StatusCode((int)HttpStatusCode.OK, new ApiResponse<dynamic>()
                {
                    Status = CommonStatusCode.STATUS_CODE_FAILED,
                    Message = message

                }
                    
                );
            }

            var profile = await _context.Profiles.FindAsync(id);
            if (profile == null)
            {
                return StatusCode((int)HttpStatusCode.NotFound, new ApiResponse<Profile>(profile,
                    CommonStatusCode.STATUS_CODE_FAILED, CommonStatusCode.NOT_FOUND_ERROR_MESSAGE
                ));
            }

            _context.Profiles.Remove(profile);
            await _context.SaveChangesAsync();

            return StatusCode((int)HttpStatusCode.OK, new ApiResponse<Profile>(profile,
                CommonStatusCode.STATUS_CODE_SUCCESS, CommonStatusCode.STATUS_CODE_SUCCESS_MESSAGE
            ));
        }

        private bool ProfileExists(int id)
        {
            return _context.Profiles.Any(e => e.ProfileId == id);
        }
    }
}