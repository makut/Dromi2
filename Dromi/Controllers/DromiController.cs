﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dromi.Logger;
using Dromi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Dromi.Controllers
{
    [Route("dromiApi")]
    [ApiController]
    public class DromiController : Controller
    {
        private readonly DromiContext _context;
        //private Mock<DromiController> factoryMock;

        public DromiController(DromiContext context)
        {
            _context = context;
        }
       


        ///<summary>
        ///Get all the profile for a particular category using the category Id
        ///e.g Selecting Fashion category will display all profiles under that fashion
        ///</summary>
        [HttpGet("profilebycategory/{availableCategoryId}")]
        [SwaggerOperation(Tags = new[] { "Dromi API" })] 
        public AvailableCategories GetProfilesByAvailableCategoryId(int availableCategoryId)
        {
            using (var data = _context)
            {
                var profile = _context.AvailableCategories.Find(availableCategoryId);
                if (profile.Equals(null))
                {
                    DromiLogs.Info<DromiController>($"Available Category is null", profile);

                }
                else
                {
                    DromiLogs.Info<DromiController>($"{profile}", profile);
                }
               data.Entry(profile).Collection(pro => pro.Profiles).Load();
                foreach(var reviews in profile.Profiles)
                {
                    _context.Entry(reviews).Collection(r => r.Reviews).Load();
                }
               
                return profile;

            }
            // var data = _context.AvailableCategories.Find(categoryId);

            //return data.profiles.ToList();
        }
        [HttpGet("AllProfiles")]
        [SwaggerOperation(Tags = new[] { "Dromi API" })]

        public List<Profile> GetAllProfiles()
        {
            var result = _context.Profiles.ToList();
            return result;
            // var data = _context.AvailableCategories.Find(categoryId);

            //return data.profiles.ToList();
        }
        ///<summary>
        ///List all the available category 
        ///</summary>
        [HttpGet("availabelCategory")]
        [SwaggerOperation(Tags = new[] { "Dromi API" })]
        public List<AvailableCategories> GetAllAvailableCategories()
        {
            using(var data= _context)
            {
               var availCat= data.AvailableCategories.Include(item => item.Profiles).ToList();
                return availCat;

            }  
        }
        // GET api/<controller>/5
        ///<summary>
        ///Get a particular profile using the profile Id
        ///</summary>
        [HttpGet("{profileId}", Name = "profile")]
        [SwaggerOperation(Tags = new[] { "Dromi API" })]
        public Profile GetProfileById(int profileId)
        {
            using (var data = _context)
            {
                var profile = _context.Profiles.Find(profileId);
                 data.Entry(profile).Collection(pro => pro.Reviews).Load();
                return profile;

            }
           
        }

        // POST api/<controller>
        ///<summary>
        ///Add or update a profile
        ///</summary>
        [HttpPost("addOrUpdateProfile")]
        [SwaggerOperation(Tags = new[] { "Dromi API" })]    
        public Profile AddOrUpdateProfile([FromBody]Profile item)
        {
            if (item.ProfileId == 0)
            {
                _context.Profiles.Add(item);
            }

            else
                _context.Profiles.Update(item);
            
            _context.SaveChanges();
            return item;

        }
        ///<summary>
        ///Add or update available category
        ///</summary>
        [HttpPost("addOrUpdateAvailableCategory")]
        [SwaggerOperation(Tags = new[] { "Dromi API" })]
        public AvailableCategories AddorUpdateAvailableCategory([FromBody]AvailableCategories item)
        {
            // var categoryUpdate = _context.AvailableCategories.Find(id);
            if (item.AvailableCategoriesId == 0)
            {
                _context.AvailableCategories.Add(item);
            }
            else
                _context.AvailableCategories.Update(item);
  
            _context.SaveChanges();
            return item;
        }
        ///<summary>
        ///Add or update category
        ///</summary>
        [HttpPost("addOrUpdateCategory")]
        [SwaggerOperation(Tags = new[] { "Dromi API" })]
        public Categories AddOrUpdateCategory([FromBody]Categories item)
        {
            if (item.CategoriesId == 0)
            {
                _context.Category.Add(item);
                DromiLogs.Info<DromiController>($"you have added {item}", item);
            }
            else
                _context.Category.Update(item);
            DromiLogs.Info<DromiController>($"you have updated {item}", item);

            _context.SaveChanges();
            return item;
        }
        [HttpPost("AddOrUpdateGallery")]
        [SwaggerOperation(Tags = new[] { "Dromi API" })]
        public Gallery AddOrUpdateGallery([FromBody]Gallery item)
        {
            if (item.GalleryId == 0)
            {
                _context.Images.Add(item);
            }
            else
                _context.Images.Update(item);

            _context.SaveChanges();
            return item;
        }
        ///<summary>
        ///Add or update review
        ///</summary>
        [HttpPost("addOrUpdateReview")]
        [SwaggerOperation(Tags = new[] { "Dromi API" })]
        public Reviews AddorUpdateReview([FromBody]Reviews item)
        {
            if (item.ReviewsId == 0)
            {
                _context.Review.Add(item);
            }
            else
                _context.Review.Update(item);

            _context.SaveChanges();
            return item;
        }
        ///<summary>
        ///Delete profile using the profile ID
        ///</summary>
        [HttpDelete("deleteProfile/{profileId}")]
        [SwaggerOperation(Tags = new[] { "Dromi API" })]
        public Profile DeleteProfileById(int profileId)
        {
            var remove = _context.Profiles.Find(profileId);
           var delete=  _context.Profiles.Remove(remove);
            return remove;

        }
        ///<summary>
        ///Delete category using the category ID
        ///</summary>
        [HttpDelete("deleteCategory/{categoryId}")]
        [SwaggerOperation(Tags = new[] { "Dromi API" })]
        public Categories DeleteCategory(int categoryId)
        {
            var remove = _context.Category.Find(categoryId);
            var delete = _context.Category.Remove(remove);
            return remove;

        }
        ///<summary>
        ///Delete available categoru using the it's ID
        ///</summary>
        [HttpDelete("deleteAvailableCategory/{id}")]
        [SwaggerOperation(Tags = new[] { "Dromi API" })]
        public AvailableCategories DeleteAvailableCategory(int availableCategoryId)
        {
            var remove = _context.AvailableCategories.Find(availableCategoryId);
            var delete = _context.AvailableCategories.Remove(remove);
            return remove;

        }
        [HttpDelete("DeleteGallery/{id}")]
        [SwaggerOperation(Tags = new[] { "Dromi API" })]
        public Gallery DeleteGallery(int galleryId)
        {
            var remove = _context.Images.Find(galleryId);
            _context.Images.Remove(remove);
            return remove;

        }

    }

    internal class SwaggerOperationAttribute : Attribute
    {
        public string[] Tags { get; set; }
    }
}
