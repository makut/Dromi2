﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Dromi.Commons;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Dromi.Models;

namespace Dromi.Controllers
{
    [Route("reviews")]
    [ApiController]
    public class ReviewsController : ControllerBase
    {
        private readonly DromiContext _context;

        public ReviewsController(DromiContext context)
        {
            _context = context;
        }

        // GET: api/Reviews
        [HttpGet]
        public IEnumerable<Reviews> GetReview()
        {
            return _context.Review;
        }

        // GET: api/Reviews/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetReviews([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<dynamic>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED
                });
            }

            var reviews = await _context.Review.FindAsync(id);

            if (reviews == null)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.NotFound, new ApiResponse<dynamic>()
                {
                    Message = CommonStatusCode.NOT_FOUND_ERROR_MESSAGE,
                    Status = CommonStatusCode.STATUS_CODE_FAILED
                });
            }

            return Ok(reviews);
        }

        // PUT: api/Reviews/5
        [HttpPut("{id}")]
        public async Task<IActionResult> update([FromRoute] int id, [FromBody] Reviews reviews)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<dynamic>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED
                });
            }

            if (id != reviews.ReviewsId)
            {
               // var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<dynamic>()
                {
                    Message = "Id is not correct",
                    Status = CommonStatusCode.STATUS_CODE_FAILED
                });
            }

            _context.Entry(reviews).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReviewsExists(id))
                {
                    var message = ModelState.GetErrorMessages(",");
                    return StatusCode((int)HttpStatusCode.NotFound, new ApiResponse<dynamic>()
                    {
                        Message = CommonStatusCode.NOT_FOUND_ERROR_MESSAGE,
                        Status = CommonStatusCode.STATUS_CODE_FAILED
                    });
                }
                else
                {
                    throw;
                }
            }

            return StatusCode((int)HttpStatusCode.OK, new ApiResponse<dynamic>()
            {
                Message = CommonStatusCode.STATUS_CODE_SUCCESS_MESSAGE,
                Status = CommonStatusCode.STATUS_CODE_SUCCESS
            }); ;
        }

        // POST: api/Reviews
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Reviews reviews)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<dynamic>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED
                });
            }

            _context.Review.Add(reviews);
            await _context.SaveChangesAsync();

            return StatusCode((int)HttpStatusCode.OK, new ApiResponse<Reviews>()
            {
                Message = CommonStatusCode.STATUS_CODE_SUCCESS,
                Status = CommonStatusCode.STATUS_CODE_SUCCESS_MESSAGE
            });
        }

        // DELETE: api/Reviews/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteReviews([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<dynamic>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED
                });
            }

            var reviews = await _context.Review.FindAsync(id);
            if (reviews == null)
            {
                //var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<dynamic>()
                {
                    Message = CommonStatusCode.NOT_FOUND_ERROR_MESSAGE,
                    Status = CommonStatusCode.STATUS_CODE_FAILED
                });
            }

            _context.Review.Remove(reviews);
            await _context.SaveChangesAsync();

            return Ok(reviews);
        }

        private bool ReviewsExists(int id)
        {
            return _context.Review.Any(e => e.ReviewsId == id);
        }
    }
}