﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Commons;
using Dromi.Commons;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Dromi.Models;
using Npgsql;

//using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Dromi.Controllers
{
    [Route("availableCategory")]
    [ApiController]
    public class AvailableCategoriesController : ControllerBase
    {
        private readonly DromiContext _context;
        private AvailableCategories categories;


        public AvailableCategoriesController(DromiContext context)
        {
            _context=context;
        }

        // GET: api/AvailableCategories
        [HttpGet]
        public IEnumerable<AvailableCategories> GetAvailableCategories()
        {

            var category = _context.AvailableCategories.Include(r => r.Profiles);

            return category;
        }
       

        // GET: api/AvailableCategories/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAvailableCategories([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<AvailableCategories>()
                {
                    Status = CommonStatusCode.STATUS_CODE_FAILED,
                    Message = message
                });
                //DromiLogs.Info<AvailableCategories>($"Available Categnory is null", "");
            }

            using (var data=_context)
            {
                var getProfile = data.AvailableCategories.Find(id);
                if (getProfile==null)
                {
                    DromiLogs.Info<AvailableCategoriesController>("Available category Id is not correct or item null");
                    return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<AvailableCategories>()
                    {
                        Status = CommonStatusCode.STATUS_CODE_FAILED,
                        Message = "Id is not correct or item not found"
                    });
                }

                if (id!=getProfile.AvailableCategoriesId)
                {
                    return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<dynamic>()
                    {
                        Status = CommonStatusCode.STATUS_CODE_FAILED,
                        Message = "Id is not correct"

                    });

                }
                data.Entry(getProfile).Collection(pro => pro.Profiles).Load();
               
                return StatusCode((int)HttpStatusCode.OK, new ApiResponse<AvailableCategories>(getProfile,
                    CommonStatusCode.STATUS_CODE_SUCCESS, CommonStatusCode.STATUS_CODE_SUCCESS_MESSAGE
                ));
            }
            
           
          

        }

        // PUT: api/AvailableCategories/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] AvailableCategories availableCategories)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int) HttpStatusCode.BadRequest, new ApiResponse<AvailableCategories>()
                {
                    Message = message,
                    Status=CommonStatusCode.STATUS_CODE_FAILED
                    
                });
            }

            if (id != availableCategories.AvailableCategoriesId)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<AvailableCategories>()
                {
                    Message = "Id is not correct",
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            _context.Entry(availableCategories).State = EntityState.Modified;

            try
            {
               await _context.SaveChangesAsync();
               
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AvailableCategoriesExists(id))
                {
                    return StatusCode((int) HttpStatusCode.NotFound, new ApiResponse<AvailableCategories>(
                        availableCategories,
                        CommonStatusCode.STATUS_CODE_FAILED, CommonStatusCode.NOT_FOUND_ERROR_MESSAGE));
                }
                else
                {
                    throw;
                }
            }

            return StatusCode((int)HttpStatusCode.OK);
        }

        // POST: api/AvailableCategories
        [HttpPost ]
        public async Task<IActionResult> Create([FromBody] AvailableCategories availableCategories)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");

                return StatusCode((int) HttpStatusCode.BadRequest, new ApiResponse<dynamic>
                {
                    Status = CommonStatusCode.STATUS_CODE_FAILED,
                    Message = message
                });
            }
           

            try
            {
               _context.AvailableCategories.Add(availableCategories);
                await _context.SaveChangesAsync();
                return StatusCode((int)HttpStatusCode.Created, new ApiResponse<AvailableCategories>(availableCategories,
                    CommonStatusCode.STATUS_CODE_SUCCESS,
                    CommonStatusCode.STATUS_CODE_SUCCESS));
            }

            catch (DbUpdateException ex)
            {
                DromiLogs.Error<AvailableCategoriesController>(ex, ex.StackTrace);

                var pgException = (PostgresException)ex.InnerException;

                if (pgException != null)
                {
                    var exResponse = new ApiResponse<string>(null,
                        CommonStatusCode.STATUS_CODE_FAILED,
                        pgException.Detail);
                    return StatusCode((int)HttpStatusCode.InternalServerError, exResponse);
                }

                var response = new ApiResponse<string>(null,
                    CommonStatusCode.STATUS_CODE_FAILED,
                    ex.InnerException.ToString());
                return StatusCode((int)HttpStatusCode.InternalServerError, response);
            }
            catch (Exception ex)
            {
                DromiLogs.Error<AvailableCategoriesController>(ex, ex.ToString());
                var response = new ApiResponse<string>(string.Empty,
                    CommonStatusCode.STATUS_CODE_FAILED,
                    CommonStatusCode.INTERNAL_SERVER_ERROR_MESSAGE);
                return
                    StatusCode((int)HttpStatusCode.InternalServerError, response);

            }

        }


        private bool AvailableCategoriesExists(int id)
        {
            return _context.AvailableCategories.Any(e => e.AvailableCategoriesId == id);
        }
    }
}