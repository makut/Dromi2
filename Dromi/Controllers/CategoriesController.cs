﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Dromi.Models;
using Dromi.Commons;
using Commons;
using Npgsql;

namespace Dromi.Controllers
{
    [Route("category")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly DromiContext _context;

        public CategoriesController(DromiContext context)
        {
            _context = context;
        }

        // GET: api/Categories
        [HttpGet]
        public IEnumerable<Categories> GetCategories()
        {
            return _context.Category;
        }

       
        [HttpPut("{id}")]
        public async Task<IActionResult> update([FromRoute] int id, [FromBody] Categories categories)
        {


            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<AvailableCategories>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            if (id != categories.CategoriesId)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<AvailableCategories>()
                {
                    Message = "Id is not correct",
                    Status = CommonStatusCode.STATUS_CODE_FAILED

                });
            }

            _context.Entry(categories).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoriesExists(id))
                {
                    return StatusCode((int)HttpStatusCode.NotFound, new ApiResponse<Categories>(
                        categories,
                        CommonStatusCode.STATUS_CODE_FAILED, CommonStatusCode.NOT_FOUND_ERROR_MESSAGE));
                }
                else
                {
                    throw;
                }
            }

            return StatusCode((int)HttpStatusCode.OK, new ApiResponse<Categories>(categories,
                CommonStatusCode.STATUS_CODE_SUCCESS,CommonStatusCode.STATUS_CODE_SUCCESS_MESSAGE
                ));
        }

        // POST: api/Categories
        [HttpPost]
        public  async  Task <IActionResult> Create([FromBody] Categories categories)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");

                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<dynamic>
                {
                    Status = CommonStatusCode.STATUS_CODE_FAILED,
                    Message = message
                });
            }


            try
            {
                _context.Category.Add(categories);
                 await _context.SaveChangesAsync();
                return StatusCode((int)HttpStatusCode.Created, new ApiResponse<Categories>(categories,
                    CommonStatusCode.STATUS_CODE_SUCCESS,
                    CommonStatusCode.STATUS_CODE_SUCCESS));
            }

            catch (DbUpdateException ex)
            {
                DromiLogs.Error<CategoriesController>(ex, ex.StackTrace);

                var pgException = (PostgresException)ex.InnerException;

                if (pgException != null)
                {
                    var exResponse = new ApiResponse<string>(null,
                        CommonStatusCode.STATUS_CODE_FAILED,
                        pgException.Detail);
                    return StatusCode((int)HttpStatusCode.InternalServerError, exResponse);
                }

                var response = new ApiResponse<string>(null,
                    CommonStatusCode.STATUS_CODE_FAILED,
                    ex.InnerException.ToString());
                return StatusCode((int)HttpStatusCode.InternalServerError, response);
            }
            catch (Exception ex)
            {
                DromiLogs.Error<CategoriesController>(ex, ex.ToString());
                var response = new ApiResponse<string>(string.Empty,
                    CommonStatusCode.STATUS_CODE_FAILED,
                    CommonStatusCode.INTERNAL_SERVER_ERROR_MESSAGE);

                return StatusCode((int)HttpStatusCode.InternalServerError, response);

            }
        }

        // DELETE: api/Categories/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCategories([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");

                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<dynamic>
                {
                    Status = CommonStatusCode.STATUS_CODE_FAILED,
                    Message = message
                });
            }

            var categories = await _context.Category.FindAsync(id);
            if (categories == null)
            {
                return StatusCode((int)HttpStatusCode.NotFound, new ApiResponse<Categories>(
                    categories,
                    CommonStatusCode.STATUS_CODE_FAILED, CommonStatusCode.NOT_FOUND_ERROR_MESSAGE));
            }

            _context.Category.Remove(categories);
            await _context.SaveChangesAsync();

            return StatusCode((int)HttpStatusCode.OK, new ApiResponse<Categories>(
                categories,
                CommonStatusCode.STATUS_CODE_SUCCESS, CommonStatusCode.STATUS_CODE_SUCCESS_MESSAGE));
        }

        private bool CategoriesExists(int id)
        {
            return _context.Category.Any(e => e.CategoriesId == id);
        }
    }
}