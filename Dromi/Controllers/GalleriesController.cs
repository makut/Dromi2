﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Dromi.Commons;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Dromi.Models;

namespace Dromi.Controllers
{
    [Route("gallery")]
    [ApiController]
    public class GalleriesController : ControllerBase
    {
        private readonly DromiContext _context;

        public GalleriesController(DromiContext context)
        {
            _context = context;
        }

        // GET: api/Galleries
        [HttpGet]
        public IEnumerable<Gallery> GetImages()
        {
            return _context.Images;
        }

        // GET: api/Galleries/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetGallery([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int) HttpStatusCode.BadRequest, new ApiResponse<dynamic>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED
                });
            }


            var gallery = await _context.Images.FindAsync(id);

                if (gallery == null)
                {
                    return StatusCode((int)HttpStatusCode.NotFound, new ApiResponse<Gallery>()
                    {
                        Status = CommonStatusCode.NOT_FOUND_ERROR_MESSAGE,
                        Message = "not found"
                    });
                }

                return StatusCode((int)HttpStatusCode.OK, new  ApiResponse<Gallery>());
            }
        

        // PUT: api/Galleries/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Update([FromRoute] int id, [FromBody] Gallery gallery)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<dynamic>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED
                });
            }

            if (id != gallery.GalleryId)
            {
                //var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<dynamic>()
                {
                    Message = "id is not valid",
                    Status = CommonStatusCode.STATUS_CODE_FAILED
                });
            }

            _context.Entry(gallery).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GalleryExists(id))
                {
                    var message = ModelState.GetErrorMessages(",");
                    return StatusCode((int)HttpStatusCode.NotFound, new ApiResponse<dynamic>()
                    {
                        Message = "",
                        Status = CommonStatusCode.STATUS_CODE_FAILED
                    });
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Galleries
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] Gallery gallery)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<dynamic>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED
                });
            }

            _context.Images.Add(gallery);
            await _context.SaveChangesAsync();

            return StatusCode((int)HttpStatusCode.OK);
        }

        // DELETE: api/Galleries/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGallery([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<dynamic>()
                {
                    Message = message,
                    Status = CommonStatusCode.STATUS_CODE_FAILED
                });
            }

            var gallery = await _context.Images.FindAsync(id);
            if (gallery == null)
            {
                //var message = ModelState.GetErrorMessages(",");
                return StatusCode((int)HttpStatusCode.BadRequest, new ApiResponse<dynamic>()
                {
                    Message = CommonStatusCode.NOT_FOUND_ERROR_MESSAGE,
                    Status = CommonStatusCode.STATUS_CODE_FAILED
                });
            }

            _context.Images.Remove(gallery);
            await _context.SaveChangesAsync();

            return StatusCode((int)HttpStatusCode.OK);
        }

        private bool GalleryExists(int id)
        {
            return _context.Images.Any(e => e.GalleryId == id);
        }
    }
}