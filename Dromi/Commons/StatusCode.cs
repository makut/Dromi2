﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dromi.Commons
{
    public class CommonStatusCode
    {

        /// <summary>
        /// Failed status key
        /// </summary>
        public static readonly string STATUS_CODE_FAILED = "failed";

        /// <summary>
        /// Error status key
        /// </summary>
        public static readonly string STATUS_CODE_ERROR = "error";

        /// <summary>
        /// Success status key
        /// </summary>
        public static readonly string STATUS_CODE_SUCCESS = "success";

        /// <summary>
        ///
        /// </summary>
        public static readonly string INTERNAL_SERVER_ERROR_MESSAGE = "Internal Server Error";

        /// <summary>
        ///
        /// </summary>
        public static readonly string NOT_FOUND_ERROR_MESSAGE = "Item not found";
        public static readonly string STATUS_CODE_SUCCESS_MESSAGE = "success";

        /// <summary>
        ///
        /// </summary>
    }
}
